#!/usr/bin/python

from ansible.module_utils.basic import *

module = AnsibleModule(argument_spec={})
response = {"hello": "world"}
module.exit_json(changed=False, meta=response)
